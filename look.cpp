#include <iostream>
#include <cstdio>
#include <vector>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <ctime>
#include <string>
#include <sstream>

using namespace std;


int dfs(int ind, int n, int k, int s, string& str) {
   /*
	��������, �������� ������� ������ �� �������� � ��������� �������
	*/
	if (ind == n) 
       return 1;
	if (ind - k + 1 < 0) {
       int ans = 0;
       for (int i = 0; i < 10; i++)
           if (str[ind] == '?' || str[ind] == '0' + i) {
               char p = str[ind];
               str[ind] = '0' + i;
               ans += dfs(ind + 1, n, k, s, str);
               str[ind] = p;
           }
       return ans;
   }

   /*
	��������, ������ �������� ���������� �������� � ������� � ����������
	*/
	int cursum = 0;
   for (int i = ind - k + 1; i < ind; i++)
       cursum += str[i] - '0';

   int curD = s - cursum;
   if (curD < 0 || curD >= 10)
       return 0;
   
   if (str[ind] != '?' && str[ind] != curD + '0')
       return 0;

   char p = str[ind];
   str[ind] = '0' + curD;
   int ans = dfs(ind + 1, n, k, s, str);
   str[ind] = p;
   return ans;
}

void solve() {
   int n, k, s;
   cin >> n >> k >> s;
   string str;
   cin >> str;

   cout << dfs(0, n, k, s, str) << endl;
}

int main() {
   solve();
   return 0;
}